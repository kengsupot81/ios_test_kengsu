//
//  ios_test_kengsuApp.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

@main
struct ios_test_kengsuApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
