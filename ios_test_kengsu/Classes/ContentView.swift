//
//  ContentView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct ContentView: View {
    @State private var selectedTab = "MVVM"
    var body: some View {
        TabView(selection: $selectedTab) {
            MainView()
                .tag("Main")
                .tabItem {
                    Image(systemName: "scribble.variable")
                    Text("Main")
                }

            OtherView()
                .tag("Other")
                .tabItem {
                    Image(systemName: "lasso.and.sparkles")
                    Text("Other")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
