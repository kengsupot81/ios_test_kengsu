//
//  Worker.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import Foundation

struct Worker {
    func getData(url_request: URL?) async throws -> TestData {
        guard let url = url_request else { fatalError("Missing URL") }

        let (data, response) = try await URLSession.shared.data(from: url)

        guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError("Request error") }

        let decodedUsers = try JSONDecoder().decode(TestData.self, from: data)
        return decodedUsers
    }

    func convertCurrency(rates: [String: Double], value: [String: Double], completion: @escaping ([String: Double]?) -> ()) {
        guard let key = value.keys.first, let value = value.values.first else { return }
        var changeValue: [String: Double] = [:]
        if key == "BTC" {
            changeValue["BTC"] = value
        } else {
            changeValue["BTC"] = Double(value / rates[key]!)
        }

        for item in rates {
            changeValue[item.key] = Double(item.value * changeValue["BTC"]!).rounded(digits: 4)
        }

        completion(changeValue)
    }
}
