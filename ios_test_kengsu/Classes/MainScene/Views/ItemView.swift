//
//  ItemView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct ItemView: View {
    var item: TestData.BPI

    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(self.item.description)
                    .font(.body)
                    .fontWeight(.bold)
                Text("( \(self.item.code) )")
                    .font(.body)
            }

            Spacer()

            Text(self.item.rate.currencyFormat(code: self.item.code))
                .font(.body)
                .fontWeight(.bold)
                .foregroundColor(Color.green)
        }
        .padding([.top, .bottom], 5.0)
        .padding([.leading, .trailing], 20.0)
        .background(
            RoundedRectangle(cornerRadius: 20, style: .continuous)
                .fill(Color.white)
        )
    }
}
