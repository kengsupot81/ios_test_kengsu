//
//  ConversionRateView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct ConversionRateView: View {
    var currentData: TestData

    var body: some View {
        Section(header: Text("Conversion rate.").font(.body).fontWeight(.bold)) {
            let items: [String: TestData.BPI] = self.currentData.bpi
            ForEach(Array(items.keys.sorted().enumerated()), id: \.element) { _, key in
                let item: TestData.BPI = items[key]!
                ItemView(item: item)
            }
        }
    }
}
