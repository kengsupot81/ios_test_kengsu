//
//  ConvertCurrencyView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct ConvertCurrencyView: View {
    var titleText: String
    var main: Bool = false
    @Binding var valueText: String

    var body: some View {
        VStack {
            HStack {
                Text(self.titleText)
                    .font(.system(size: (self.main) ? 20.0 : 16.0))
                    .fontWeight((self.main) ? .bold : .regular)
                Spacer()
                TextField("0.0", text: self.$valueText)
                    .multilineTextAlignment(.trailing)
                    .keyboardType(.decimalPad)
            }
        }
    }
}
