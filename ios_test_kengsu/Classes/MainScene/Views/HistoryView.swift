//
//  HistoryView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct HistoryView: View {
    let itemCurrent: TestData
    let itemHistory: [TestData]

    var body: some View {
        Section(header: Text("History").font(.title).fontWeight(.bold)) {
            ItemHistoryView(timeText: itemCurrent.time["updated"] ?? "", current: true, items: itemCurrent.bpi).padding(10.0)
            Divider()
            List {
                if let items = self.itemHistory, items.count > 0 {
                    ForEach(0 ..< items.count, id: \.self) { index in
                        let item = items[index]
                        ItemHistoryView(timeText: item.time["updated"] ?? "", items: item.bpi)
                    }
                }
            }
        }
    }
}
