//
//  HeaderView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct HeaderView: View {
    let itemCurrent: TestData
    let loadingMode: Bool
    @Binding var isPresentHistory: Bool

    var body: some View {
        HStack(alignment: .top) {
            Text(self.itemCurrent.chartName)
                .font(.title)
                .fontWeight(.heavy)
                .padding(5.0)
                .textCase(.uppercase)

            Spacer()

            VStack(alignment: .trailing) {
                Text(self.itemCurrent.time["updated"] ?? "").font(.system(size: 10.0))
                HStack(spacing: 10.0) {
                    if self.loadingMode {
                        ProgressView()
                    }

                    Button("History") {
                        self.isPresentHistory = true
                    }
                    .buttonStyle(.borderedProminent)
                }
            }
        }
        .padding()
        .background(BackgroundView())
    }
}
