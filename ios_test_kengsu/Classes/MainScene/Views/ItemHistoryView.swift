//
//  ItemHistoryView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct ItemHistoryView: View {
    var timeText: String
    var current: Bool = false
    var items: [String: TestData.BPI]

    var body: some View {
        Section(header:
            HStack {
                Text(self.timeText).font(.body)
                Spacer()
                if current {
                    Text("Current").font(.body).foregroundColor(Color.green)
                }
            }
        ) {
            VStack {
                let items: [String: TestData.BPI] = self.items
                ForEach(Array(items.keys.sorted().enumerated()), id: \.element) { _, key in
                    let item: TestData.BPI = items[key]!
                    HStack {
                        Text(item.description)
                            .font(.body)
                            .fontWeight(.bold)

                        Spacer()

                        Text(item.rate.currencyFormat(code: item.code))
                            .font(.body)
                    }
                    .padding(5.0)
                }
            }
        }
    }
}
