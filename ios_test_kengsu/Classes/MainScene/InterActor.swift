//
//  InterActor.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import Foundation

protocol InterActorInterface {
    func doFetchData(request: Interface.GetData.Request)
    func doConvertCurrency(request: Interface.ConvertCurrency.Request)
}

class InterActor: InterActorInterface {
    var presenter: Presenter!

    func doFetchData(request: Interface.GetData.Request) {
        Task { @MainActor in
            do {
                let result = try await Worker().getData(url_request: request.url)
                let response = Interface.GetData.Response(testData: result)
                self.presenter?.didFetchData(response: response)
            } catch {
                print("Error : ", error.localizedDescription)
            }
        }
    }

    func doConvertCurrency(request: Interface.ConvertCurrency.Request) {
        Worker().convertCurrency(rates: request.rate, value: request.value) { request in
            let response = Interface.ConvertCurrency.Response(convertValue: request)
            self.presenter?.didConvertCurrency(response: response)
        }
    }
}
