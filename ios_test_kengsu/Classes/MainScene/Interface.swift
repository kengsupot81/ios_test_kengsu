//
//  Interface.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import Foundation

enum Interface {
    enum GetData {
        struct Request {
            let url: URL
        }

        struct Response {
            let testData: TestData?
        }
    }

    enum ConvertCurrency {
        struct Request {
            let rate: [String: Double]
            let value: [String: Double]
        }

        struct Response {
            let convertValue: [String: Double]?
        }
    }
}
