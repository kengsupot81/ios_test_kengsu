//
//  Controller.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import Foundation

class Controller {
    public weak var viewModel: ViewModel!

    private var interActor: InterActorInterface!
    private var presenter: PresenterInterface!

    private let url: URL! = .init(string: "https://api.coindesk.com/v1/bpi/currentprice.json")
    private let timeFetch: CGFloat = 60.0

    init(viewModel: ViewModel) {
        self.viewModel = viewModel
        self.configCleanArchitecture()

        self.startLoadData()
    }

    deinit {}

    private func configCleanArchitecture() {
        let interActor: InterActor = .init()
        let presenter: Presenter = .init()

        interActor.presenter = presenter
        presenter.controller = self

        self.interActor = interActor
        self.presenter = presenter
    }

    private func startLoadData() {
        Task { @MainActor in
            self.fetchData()
        }
    }

    @MainActor func fetchData() {
        self.viewModel.loadingMode = true
        let request = Interface.GetData.Request(url: self.url)
        self.interActor.doFetchData(request: request)
    }

    @MainActor func calculateConvertCurrency(valueChange: [String: String]) {
        guard let key = valueChange.keys.first, let value = valueChange.values.first else { return }

        if let items = self.viewModel.currentData?.bpi {
            var rate: [String: Double] = [:]
            items.forEach { rate[$0.key] = $0.value.rate_float }
            var tempValue: [String: Double] = [:]
            tempValue[key] = (value.isEmpty) ? 0.0 : Double(value)
            let request = Interface.ConvertCurrency.Request(rate: rate, value: tempValue)
            self.interActor.doConvertCurrency(request: request)
        }
    }
}

protocol ControllerInterface {
    func presenterFetchData(newData: TestData)
    func presenterConvertCurrency(newData: [String: String])
}

extension Controller: ControllerInterface {
    @MainActor func presenterFetchData(newData: TestData) {
        self.viewModel.loadingMode = false
        if let currentData = self.viewModel.currentData {
            self.viewModel.historyData.insert(currentData, at: 0)
        }
        self.viewModel.currentData = newData

        newData.bpi.forEach { item in
            if self.viewModel.convertText[item.value.code] == nil {
                self.viewModel.convertText[item.value.code] = ""
            }
        }
        if self.viewModel.convertText["BTC"] == nil {
            self.viewModel.convertText["BTC"] = ""
        } else {
            self.calculateConvertCurrency(valueChange: ["BTC": self.viewModel.convertText["BTC"]!])
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + self.timeFetch) {
            self.startLoadData()
        }
    }

    func presenterConvertCurrency(newData: [String: String]) {
        self.viewModel.convertText = newData
    }
}
