//
//  MainView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct MainView: View {
    @ObservedObject private var viewModel: ViewModel = .init()
    private var controller: Controller!

    @State private var shouldPresentHistory: Bool = false
    @State private var valueChange: [String: String] = [:]
    @FocusState private var isInputActive: Bool

    init() {
        self.controller = .init(viewModel: self.viewModel)
    }

    var body: some View {
        GeometryReader { _ in
            if let currentData = self.viewModel.currentData {
                VStack {
                    Section(header: HeaderView(itemCurrent: currentData,
                                               loadingMode: self.viewModel.loadingMode,
                                               isPresentHistory: self.$shouldPresentHistory)) {
                        VStack(alignment: .leading, spacing: 10.0) {
                            Group {
                                Text(currentData.disclaimer)
                                    .font(Font.system(.body))
                                    .multilineTextAlignment(.leading)
                            }

                            Divider()

                            Group {
                                ConversionRateView(currentData: currentData)
                            }
                            .padding([.leading, .trailing], 10.0)

                            Divider()

                            Spacer()

                            Group {
                                Form {
                                    List {
                                        Section(header: Text("Convert currency.").font(.system(size: 24.0)).fontWeight(.bold)) {
                                            ConvertCurrencyView(titleText: "BTC",
                                                                main: true,
                                                                valueText: Binding(
                                                                    get: { self.viewModel.convertText["BTC"] ?? "" },
                                                                    set: {
                                                                        self.valueChange = ["BTC": $0]
                                                                        self.viewModel.convertText["BTC"] = $0
                                                                    }
                                                                ))
                                                                .focused(self.$isInputActive)
                                                                .onTapGesture {
                                                                    self.viewModel.convertText["BTC"] = ""
                                                                }

                                            let items = self.viewModel.convertText.filter { $0.key != "BTC" }
                                            ForEach(Array(items.keys.sorted().enumerated()), id: \.element) { _, key in
                                                ConvertCurrencyView(titleText: key,
                                                                    valueText: Binding(
                                                                        get: { self.viewModel.convertText[key] ?? "" },
                                                                        set: {
                                                                            self.valueChange = [key: $0]
                                                                            self.viewModel.convertText[key] = $0
                                                                        }
                                                                    ))
                                                                    .focused(self.$isInputActive)
                                                                    .onTapGesture {
                                                                        self.viewModel.convertText[key] = ""
                                                                    }
                                            }
                                        }
                                    }
                                }
                                .toolbar {
                                    ToolbarItemGroup(placement: .keyboard) {
                                        Spacer()
                                        Button("Done") {
                                            self.controller.calculateConvertCurrency(valueChange: self.valueChange)
                                            self.isInputActive = false
                                        }
                                    }
                                }
                            }
                        }
                        .padding(5.0)
                    }
                }
                .sheet(isPresented: self.$shouldPresentHistory, content: {
                    HistoryView(itemCurrent: currentData,
                                itemHistory: self.viewModel.historyData)
                })
            }
        }
        .background(BackgroundView())
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
