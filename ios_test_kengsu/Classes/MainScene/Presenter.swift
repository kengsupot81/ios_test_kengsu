//
//  Presenter.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import Foundation

protocol PresenterInterface {
    func didFetchData(response: Interface.GetData.Response)
    func didConvertCurrency(response: Interface.ConvertCurrency.Response)
}

class Presenter: PresenterInterface {
    weak var controller: Controller!

    @MainActor func didFetchData(response: Interface.GetData.Response) {
        guard let newData = response.testData else { return }
        self.controller.presenterFetchData(newData: newData)
    }

    func didConvertCurrency(response: Interface.ConvertCurrency.Response) {
        guard let items = response.convertValue else { return }

        var newData: [String: String] = [:]
        items.forEach {
            let number = NSNumber(value: $0.value).decimalValue
            newData[$0.key] = (number == 0) ? "" : String(describing: number)
        }

        self.controller.presenterConvertCurrency(newData: newData)
    }
}
