//
//  ViewModel.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import Foundation

class ViewModel: ObservableObject {
    @Published var currentData: TestData?
    @Published var historyData: [TestData] = []
    @Published var loadingMode: Bool = false

    @Published var convertText: [String: String] = [:]
}

struct TestData: Decodable {
    var time: [String: String] = [:]
    var disclaimer: String = ""
    var chartName: String = ""
    var bpi: [String: BPI] = [:]

    struct BPI: Decodable {
        var code: String
        var symbol: String
        var rate: String
        var description: String
        var rate_float: Double
    }
}
