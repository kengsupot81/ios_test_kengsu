//
//  OtherView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct OtherView: View {
    @State private var fibonacciText: String = ""
    @State private var primeNumberText: String = ""
    @FocusState private var isInputActive: Bool

    @State private var temp1Data: [Int] = []
    @State private var temp2Data: [Int] = []
    @State private var temp3Data: [Int] = []

    var body: some View {
        GeometryReader { _ in
            VStack {
                Section(header: Text("Other").font(.title).fontWeight(.bold)) {
                    List {
                        Group {
                            Section(header:
                                HStack {
                                    Text("1.").font(.body).fontWeight(.bold)
                                    Spacer()
                                    HStack(spacing: 10) {
                                        Text("Fibonacci").font(.body).fontWeight(.bold)
                                        TextField("0", text: self.$fibonacciText)
                                            .multilineTextAlignment(.trailing)
                                            .keyboardType(.numberPad)
                                            .focused(self.$isInputActive)
                                            .textFieldStyle( .roundedBorder )
                                            .frame(maxWidth: 60)
                                    }
                                }
                            ) {
                                Text("[ \(self.temp1Data.map { String($0) }.joined(separator: ", ")) ]")
                            }
                        }

                        Group {
                            Section(header:
                                HStack {
                                    Text("2.").font(.body).fontWeight(.bold)
                                    Spacer()
                                    HStack(spacing: 10) {
                                        Text("Prime Number").font(.body).fontWeight(.bold)
                                        TextField("0", text: self.$primeNumberText)
                                            .multilineTextAlignment(.trailing)
                                            .keyboardType(.numberPad)
                                            .focused(self.$isInputActive)
                                            .textFieldStyle( .roundedBorder )
                                            .frame(maxWidth: 60)
                                    }
                                }
                            ) {
                                Text("[ \(self.temp2Data.map { String($0) }.joined(separator: ", ")) ]")
                            }
                        }

                        Group {
                            Section(header: Text("3.").font(.body).fontWeight(.bold)) {
                                Text("[ \(self.temp3Data.map { String($0) }.joined(separator: ", ")) ]")
                            }
                        }
                    }
                    .listStyle(.grouped)
                }
            }
            .onAppear{
                self.fibonacciSequence()
                self.primeNumberSequence()
            }
            .toolbar {
                ToolbarItemGroup(placement: .keyboard) {
                    Spacer()
                    Button("Done") {
                        self.isInputActive = false
                        self.fibonacciSequence()
                        self.primeNumberSequence()
                    }
                }
            }
        }
    }
    
    private func fibonacciSequence() {
        guard let num = Int(self.fibonacciText) else { return }
        
        self.temp1Data.removeAll()
        for index in 0...num {
            self.temp1Data.append(self.fibonacci(index))
        }
        self.compareArray()
    }
    
    private func fibonacci(_ n: Int) -> Int {
        guard n != 0, n != 1 else { return n }
        return fibonacci(n - 1) + fibonacci(n - 2)
    }
    
    private func primeNumberSequence() {
        guard let num = Int(self.primeNumberText) else { return }
        
        self.temp2Data.removeAll()
        for index in 0...num {
            let value = self.primeNumber(index)
            if value > 0 {
                self.temp2Data.append(value)
            }
        }
        self.compareArray()
    }
    private func primeNumber(_ n: Int) -> Int{
        guard n != 0, n != 1 else { return 0 }
       for index in 2..<n{
          if (n % index == 0){
             return 0
          }
       }
       return n
    }
    
    private func compareArray() {
        self.temp3Data.removeAll()
        for item1 in self.temp1Data {
            for item2 in self.temp2Data {
                if item1 == item2 {
                    self.temp3Data.append(item1)
                    break
                }
            }
        }
    }
}

struct OtherView_Previews: PreviewProvider {
    static var previews: some View {
        OtherView()
    }
}
