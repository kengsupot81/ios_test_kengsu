//
//  BackgroundView.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import SwiftUI

struct BackgroundView: View {
    var body: some View {
        LinearGradient(
            gradient:
            Gradient(colors:
                [Color.black.opacity(0.0),
                 Color.black.opacity(0.9)]),
            startPoint: .top, endPoint: .bottom
        )
    }
}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView()
    }
}
