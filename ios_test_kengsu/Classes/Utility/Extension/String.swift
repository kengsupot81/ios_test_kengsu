//
//  String.swift
//  ios_test_kengsu
//
//  Created by Keng Su on 20/3/2566 BE.
//

import Foundation

extension String {
    func currencyFormat(code: String) -> String {
        let locale = Locale.availableIdentifiers.map { Locale(identifier: $0) }.first { $0.currency?.identifier == code }
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 4
        let num = Double(self.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range: nil))
        return formatter.string(for: num) ?? self
    }
}
